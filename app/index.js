'use strict'

import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'
import {Provider} from 'react-redux'
import {createStore, combineReducers} from 'redux'
import * as reducers from './redux'
const reducer = combineReducers(reducers)
const store = createStore(reducer)

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('app')
)