import React, {Component, PropTypes} from 'react'
import {FormGroup, FormControl, Button} from 'react-bootstrap'
import {trim as _trim} from 'lodash'


export default class CommentForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired
  }

  handleSubmit = (e)=> {
    const {onSubmit} = this.props

    e.preventDefault()
    const comment = _trim(e.target.comment.value)
    if(!comment) return
    onSubmit(comment)
  }

  render() {

    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <FormGroup>
            <FormControl
              componentClass="textarea"
              placeholder="Comment"
              name="comment"
            />
          </FormGroup>
          <Button type="submit">Send</Button>
        </form>
      </div>
    )
  }
}