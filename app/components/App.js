'use strict'

import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {Navbar, FormGroup, FormControl, Button, Row, Col} from 'react-bootstrap'
import {login} from '../redux/auth'
import {addComment} from '../redux/commentsList'
import {
  trim as _trim,
  find as _find
} from 'lodash'
import PostList from './PostList'
import styles from '../vendor/css/bootstrap.min.css'

@connect(state=>({
  postsList: state.postsList,
  auth: state.auth,
  commentsList: state.commentsList,
  usersList: state.usersList
}))


export default class App extends Component {
  constructor(){
    super()
    this.state = {
      error: null
    }
  }
  static propTypes = {
    postsList: PropTypes.object.isRequired,
    commentsList: PropTypes.object.isRequired,
    usersList: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
  }

  handleLogin = (e)=> {
    const {dispatch, usersList} = this.props
    e.preventDefault()
    const name = _trim(e.target.userName.value)
    const user = _find(usersList, {name})
    if(name && !user)
      this.setState({
        error: 'User not found. Try: user1, user2, user3 or user4. '
      })

    if(!name || !user) return
    dispatch(login(user))
  }

  handleCommentSubmit = (comment)=> {
    const {dispatch} = this.props
    dispatch(addComment(comment))
  }

  render() {
    const {postsList, commentsList, usersList, auth} = this.props
    const { error } = this.state

    return (
      <div>
        <Navbar>
          <Navbar.Form pullRight>
            {!auth.loaded && (
              <form onSubmit={this.handleLogin}>
                {error && (
                  <small className="text-danger">{error}</small>
                )}
                <FormGroup>
                  <FormControl
                    type="text"
                    placeholder="Username"
                    name="userName"
                  />
                </FormGroup>
                {' '}
                <Button type="submit">Sign In</Button>
              </form>
            )}
            {auth.loaded && (
              <ul>{`Hello ${auth.user.name}`}</ul>
            )}
          </Navbar.Form>
        </Navbar>
        <div className="container">
          <Row>
            <Col xs={12}>
              <PostList
                postsList={postsList}
                commentsList={commentsList}
                usersList={usersList}
                auth={auth}
                onSubmit={this.handleCommentSubmit}
              />
            </Col>
          </Row>
        </div>

      </div>
    )
  }
}