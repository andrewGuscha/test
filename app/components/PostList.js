import React, {Component, PropTypes} from 'react'
import Post from './Post'
import {
  map as _map,
  filter as _filter
} from 'lodash'


export default class PostList extends Component {
  static propTypes = {
    postsList: PropTypes.object.isRequired,
    commentsList: PropTypes.object.isRequired,
    usersList: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,
    onSubmit: PropTypes.func.isRequired
  }

  render() {
    const {postsList, commentsList, usersList, auth, onSubmit} = this.props

    const posts = _map(postsList, (post)=>{
      const comments = _filter(commentsList, { postId: post.id})
      return (
        <Post
          key={post.id}
          post={post}
          commentsList={comments}
          usersList={usersList}
          auth={auth}
          onSubmit={onSubmit}
        />
      )
    })

    return (
      <div>
        {posts}
      </div>
    )
  }
}