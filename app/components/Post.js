import React, {Component, PropTypes} from 'react'
import Comment from './Comment'
import {map as _map} from 'lodash'
import {Button} from 'react-bootstrap'
import CommentForm from './CommentForm'


export default class Post extends Component {
  constructor(){
    super()
    this.state = {
      clicked: false
    }
  }

  static propTypes = {
    post: PropTypes.object.isRequired,
    commentsList: PropTypes.array.isRequired,
    usersList: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,
    onSubmit: PropTypes.func.isRequired
  }

  handleClick = ()=> {
    this.setState({
      clicked: true
    })
  }

  handleSubmit = (comment)=>{
    const {onSubmit, auth, post}=this.props
    this.setState({
      clicked: false
    })

    onSubmit({
      content: comment,
      userId: auth.user.id,
      postId: post.id
    })

  }

  render() {
    const {post, commentsList, usersList, auth} = this.props
    const {clicked} = this.state

    const comments = _map(commentsList, (comment)=>{
      comment.author = usersList[comment.userId]
      return (
        <Comment
          key={comment.id}
          comment={comment}
        />
      )
    })

    return (
      <div>
        <h3>{post.title}</h3>
        <p>{post.content}</p>
        {!!comments.length &&(
          <div>
            <div className="clearfix">
              <h4 className="pull-left">Comments</h4>
              <div className="pull-right text-right">
                {(!clicked || !auth.loaded) && (
                  <Button onClick={this.handleClick}>Add comment</Button>
                )}

                {clicked && !auth.loaded && (
                  <div className="text-danger"><small>Authentication is required you need to sign in</small></div>
                )}

                {clicked && auth.loaded && (
                  <CommentForm onSubmit={this.handleSubmit} />
                )}
              </div>
            </div>
            {comments}
          </div>
        )}
        <hr style={{marginTop: '50px'}}/>
      </div>
    )
  }
}