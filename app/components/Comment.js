import React, {Component, PropTypes} from 'react'
import {Row, Col} from 'react-bootstrap'


export default class Comment extends Component {
  static propTypes = {
    comment: PropTypes.object.isRequired,
  }

  render() {
    const {comment} = this.props

    return (
      <div>
        <Row>
          <Col xs={10} xsOffset={1}>
            <div><b><i><small>{comment.author.name}</small></i></b></div>
            <p><small>{comment.content}</small></p>
          </Col>
        </Row>
      </div>
    )
  }
}