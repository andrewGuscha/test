const initialState = {
  1: {
    id: 1,
    name: 'user1'
  },
  2: {
    id: 2,
    name: 'user2'
  },
  3: {
    id: 3,
    name: 'user3'
  },
  4: {
    id: 4,
    name: 'user4'
  }
}

export default function reducer(state = initialState) {
  return state
}