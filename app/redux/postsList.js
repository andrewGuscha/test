const initialState = {
  1: {
    id: 1,
    title: 'Nam in auctor ipsum, sed tincidunt massa.',
    content: 'Pellentesque porttitor, tortor eu porta fermentum, massa velit porttitor quam, at scelerisque quam arcu a felis. Duis accumsan, lectus sed auctor rhoncus, mauris lacus faucibus nibh, at pulvinar ligula lectus quis nibh. Cras vehicula, turpis nec interdum rhoncus, libero sapien sagittis ipsum, id faucibus sapien ante sed tortor. Ut risus tortor, pellentesque luctus mauris non, feugiat efficitur massa. Donec commodo orci eget metus malesuada consectetur. Vivamus molestie, ipsum eu vulputate euismod, erat nibh ornare neque, vel finibus ligula sapien ac nisl. Aliquam vehicula nunc et consequat aliquam. Nullam efficitur enim id ligula sollicitudin, vitae volutpat justo maximus. Proin imperdiet nulla sapien, vitae facilisis lorem auctor fermentum. Curabitur et varius turpis. Sed accumsan lacus sit amet leo sodales iaculis. Nam eget accumsan ligula, ut facilisis tortor.'
  },
  2: {
    id: 2,
    title: 'Morbi nec quam ante.',
    content: 'Suspendisse sagittis metus vel odio volutpat semper. Curabitur id enim varius, egestas nisi at, feugiat quam. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam molestie urna quis nulla pellentesque aliquam. Sed cursus tellus urna, fermentum convallis risus porta a. Cras viverra mollis turpis ut malesuada. Phasellus nec vestibulum arcu. Phasellus non dui elementum odio efficitur vulputate eu ac elit. Suspendisse dictum rhoncus magna, eu pretium tortor dignissim quis. Ut nec accumsan nunc.'
  },
  3: {
    id: 3,
    title: 'Nam et purus dictum, rutrum arcu id, volutpat lectus.',
    content: 'Donec velit enim, posuere mattis tellus id, facilisis luctus nunc. Aliquam at auctor velit, id placerat nisi. Aliquam a nibh lobortis lorem blandit laoreet. Donec venenatis pellentesque turpis vitae gravida. Vestibulum eget risus eget enim vehicula posuere. Aenean feugiat tortor quis ex tristique tincidunt in in ligula. Nam felis massa, dignissim vitae magna eu, sodales pretium turpis. Nunc sit amet ornare justo, sit amet gravida augue. In hac habitasse platea dictumst. Phasellus congue sem turpis, a viverra neque varius eu. Cras vel massa quis felis rutrum lobortis et varius velit. Proin varius nulla vel feugiat sagittis.'
  },
  4: {
    id: 4,
    title: 'Sed urna erat, mattis ac bibendum sed, elementum non odio.',
    content: 'Cras eu aliquam orci, non commodo leo. Donec augue nunc, sodales sit amet massa at, viverra sagittis mi. Aenean condimentum justo quis sem imperdiet, a pretium mauris venenatis. Nulla facilisi. Vestibulum lorem urna, pulvinar posuere erat sed, elementum suscipit dolor. Phasellus rutrum dignissim est. Maecenas feugiat ante viverra diam facilisis porta.'
  },
  5: {
    id: 5,
    title: 'Fusce consequat laoreet purus vel gravida.',
    content: 'Fusce lorem augue, ullamcorper vel varius cursus, congue non lacus. Nulla dapibus metus quis magna lacinia, vitae ullamcorper lacus sagittis. Suspendisse urna justo, sodales at risus ac, euismod iaculis nisi. Praesent sit amet sem suscipit, ullamcorper leo eu, suscipit leo. Nulla facilisi. Curabitur ut risus ac odio cursus fringilla nec id libero. Vivamus condimentum cursus vestibulum. Cras gravida nunc metus, a condimentum ipsum aliquet vel. Suspendisse non risus et est aliquet ultrices et quis mi.'
  }
}

export default function reducer(state = initialState) {
  return state;
}