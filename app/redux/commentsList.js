import {size as _size} from 'lodash'

const ADD_COMMENT = 'ADD_COMMENT';

const initialState = {
  1: {
    id: 1,
    userId: 4,
    content: 'Nunc sed iaculis lacus. Ut consectetur eros velit, quis suscipit libero convallis nec.',
    postId: 5
  },
  2: {
    id: 2,
    userId: 2,
    content: 'Sed odio arcu, posuere vel lobortis non, suscipit vel tellus.',
    postId: 4
  },
  3: {
    id: 3,
    userId: 1,
    content: 'Aliquam luctus libero sem, vitae elementum erat dapibus ut. Vestibulum non lacus sit amet felis cursus suscipit ut sit amet nisi.',
    postId: 5
  },
  4: {
    id: 4,
    userId: 2,
    content: 'Quisque vel ex sed urna convallis tempor. Curabitur eleifend turpis tellus, non consequat est consequat quis.',
    postId: 3
  },
  5: {
    id: 5,
    userId: 3,
    content: 'Aenean orci ante, faucibus ut metus vitae, feugiat porta augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae',
    postId: 1
  }
  ,
  6: {
    id: 6,
    userId: 4,
    content: 'Curabitur nulla justo, facilisis non lorem eu, pharetra molestie mauris.',
    postId: 2
  }
  ,
  7: {
    id: 7,
    userId: 1,
    content: 'In blandit nisl sit amet diam cursus, sit amet feugiat urna scelerisque. Suspendisse sagittis feugiat turpis et mollis.',
    postId: 1
  }
  ,
  8: {
    id: 8,
    userId: 3,
    content: 'Aliquam bibendum ipsum non nisi efficitur sagittis.',
    postId: 3
  }
  ,
  9: {
    id: 9,
    userId: 4,
    content: 'Mauris non bibendum sapien, sit amet gravida leo. Duis at diam nec nunc iaculis viverra quis in lorem. ',
    postId: 3
  }
  ,
  10: {
    id: 10,
    userId: 2,
    content: 'Cras interdum sagittis nulla eget luctus. Duis et vehicula nulla, quis molestie purus. Etiam quis lectus id velit scelerisque hendrerit.',
    postId: 1
  }
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ADD_COMMENT:
      const newId = _size(state)+1
      return Object.assign(
        {},
        state,
        {
          [newId]:{
            ...action.comment,
            id: newId
          }
        })

    default:
      return state;
  }
}

export function addComment(comment) {
  return {
    type: ADD_COMMENT,
    comment
  }
}