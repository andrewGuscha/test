const LOGIN = 'LOGIN'

const initialState = {
  loaded: false
}

export default function reducer(state = initialState, action) {

  switch (action.type) {
    case LOGIN:
      return {
        loaded: true,
        user: action.user,
      }

    default:
      return state;
  }
}

export function login(user) {
  return {
    type: LOGIN,
    user
  }
}