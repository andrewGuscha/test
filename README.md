# Mini Twitter

# Installation instruction

## Steps

* Install dependencies

```bash
$ cd [project-path]/ && npm i
```

* Start dev sever:

```bash
$ npm start
```

* Check work:
    * Direct to url `127.0.0.1:8080`. There must be home page of site
